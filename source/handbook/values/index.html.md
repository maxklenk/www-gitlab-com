---
layout: markdown_page
title: "GitLab Values"
---

## Welcome to our Values page

We value results, transparency, sharing, freedom, efficiency, frugality,
collaboration, directness, kindness, diversity, boring solutions, and quirkiness:

1. **Results**: We care about what you achieve; the code you shipped, the user you
made happy, and the team member you helped. Do not compete by proclaiming how many
hours you worked yesterday because we don't want someone who took the afternoon
off to feel like they did something wrong. Instead, celebrate the achievements of
yourself and your teammates. We want people to have the desire to ship.
1. **Transparency**: Be open about as many things as possible. By making information
public we can reduce the threshold to contribution and make collaboration easier.
An example is the [public repository of this website](https://gitlab.com/gitlab-com/www-gitlab-com/)
that also contains this [company handbook](https://about.gitlab.com/handbook/). Everything we do is public by default, for example, the [GitLab CE](https://gitlab.com/gitlab-org/gitlab-ce/issues) and [GitLab EE](https://gitlab.com/gitlab-org/gitlab-ee/issues) issue trackers, but also [marketing](https://gitlab.com/gitlab-com/marketing/issues) and [infrastructure](https://gitlab.com/gitlab-com/infrastructure/issues). Transparency creates awareness for GitLab, allows us to recruit people that care about our culture, it gets us more and faster feedback from people outside the company, and makes it easier to collaborate with them. It is also about sharing great software, documentation, examples, lessons,
and processes with the world in the spirit of open source, which we believe creates more value than it captures.
There are exceptions, material that is not public by default is documented in the [general guidelines](#general-guidelines). On a personal level, you should tell it like it is instead of putting up a poker face. Don't be afraid to admit you made a mistake or were wrong. When something went wrong it is a great opportunity to say "What’s the [kaizen](https://en.wikipedia.org/wiki/Kaizen) moment here?" and find a better way without hurt feelings.
1. **Efficiency**: We care about working on the right things, not doing more than needed,
and not duplicating work. This enables us to achieve more progress which makes our work more fulfilling.
We do the [smallest thing possible and get it out as quickly as possible](https://about.gitlab.com/2017/01/04/behind-the-scenes-how-we-built-review-apps/). If you make suggestions that can be excluded from the first iteration turn them into a separate issue that you link. Don't write a large plan, only write the first step. Close the meta issue after you defined the first iteration. Trust that you'll know better how to proceed after something is released. You're doing it right if you're slightly embarrassed by the minimal feature set shipped in the first iteration. Give short answers to verbal questions so the other party has to opportunity to ask more or move on. And keep 1 to many written communication short, as mentioned in [this HBR study](https://hbr.org/2016/09/bad-writing-is-destroying-your-companys-productivity): "A majority say that what they read is frequently ineffective because it’s too long, poorly organized, unclear, filled with jargon, and imprecise.".
1. **Frugality**: [Amazon states it best](http://www.amazon.jobs/principles) with: "Accomplish more with less. Constraints breed resourcefulness, self-sufficiency
and invention. There are no extra points for growing headcount, budget size or
fixed expense.".
1. **Collaboration**: Helping others is a priority, even when it is not related to the goals that you are trying to achieve. You are expected to ask others for
help and advice. Anyone can chime in on any subject, including people who don't work at GitLab. The person who has to do the work decides how to do it but you should always take the suggestions seriously and try to respond and explain.
1. **Directness**<a name="directness"></a>: We try to channel our inner Ben Horowitz by being [both straightforward
and kind, an uncommon cocktail of no-bullshit and no-asshole](https://medium.com/@producthunt/ben-horowitz-s-best-startup-advice-7e8c09c8de1b). Although the feedback is always about your work and not your person it will not be easy to receive it. Any past decisions and guidelines are open to questioning as long as you act in accordance with them until they are changed, we [disagree and commit](http://ryanestis.com/leadership/disagree-and-commit-to-get-things-done/).
1. **Kindness**<a name="kindness"></a>: We don't want [jerks](http://bobsutton.typepad.com/my_weblog/2006/10/the_no_asshole_.html) in our team.
Some companies say [Evaluate People Accurately, Not "Kindly"](https://www.principles.com/#Principle-100).
We're all for accurate assessment but we think it must be done in a kind way.
Give as much positive feedback as you can and do it in a public way. Give negative
feedback in the smallest setting possible, one-on-one video calls are preferred.
Clearly make negative feedback about the work itself, not the person. When giving
feedback always provide at least one clear and recent example. If a person is
going through a hard time in their personal life, then take that into account. An example of
giving positive feedback is our [thanks chat channel](https://about.gitlab.com/handbook/communication/#internal-communication).
1. **Diversity**: The community consists of people from all over the world, with different backgrounds and opinions. We hire globally and encourage hiring in a diverse set of countries. We don't discuss religion or politics because it is easy to alienate people that have a minority opinion. Feel free to mention you attended a ceremony or rally, but don't mention the religion or party. We work to make everyone feel welcome and to increase the participation of underrepresented minorities and nationalities in our community and company. An example is our sponsorship of [diversity events](https://about.gitlab.com/2016/03/24/sponsorship-update/).
1. **Boring solutions**<a name="boring-solutions"></a>: Use the most simple and boring solution for a problem. You
can always make it more complex later if that is needed. The speed of innovation
for our organization and product is constrained by the total complexity we have added
so far, so every little reduction in complexity helps.
Don't pick an interesting technology just to make your work more fun, using code that is popular will ensure many bugs are already solved and its familiarity makes it easier for others to contribute.
1. **Quirkiness**: Unexpected and unconventional things make life more interesting.
Celebrate and encourage quirky gifts, habits, behavior, and points of view. An example
is our [team call](https://about.gitlab.com/handbook/communication/#team-call) where we spend most
of our time talking about what we did in our private lives, from fire-throwing to
knitting. Open source is a great way to interact with interesting
people. We try to hire people who think work is a great way to express themselves.
